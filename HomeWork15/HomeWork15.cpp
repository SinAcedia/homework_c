﻿
#include <iostream>

//Функция возвращает все четные или нечетные число до N
//Параметры:
//  parity: true - для четных чисел, false - для нечетных чисел
//  N: Конечное число, до которого выводится значение
void WriteParityNumber(bool parity, int N)
{

    for (int i = 0; i < N; i += i % 2 + 1 + parity)
    {
        std::cout << i << " ";
    }
    std::cout << "\n\n";
}

int main()
{
    const int N = 100;
    
    for (int i = 0 ; i < N; i++)
    {   
        if (i % 2 == 0)
        {
            std::cout << i << " ";
        }
    }
    std::cout << "\n\n";

    WriteParityNumber(true, N);
    WriteParityNumber(false, N);

}



