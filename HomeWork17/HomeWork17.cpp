﻿#include <iostream>
class MyClass
{

private:
    int myParam1;
    double myParam2;
    bool myParam3;

public:
    MyClass() : myParam1(0), myParam2(0.0),myParam3(false)
    { }
    MyClass(int _myParam1, double _myParam2, bool _myParam3) : myParam1(_myParam1), myParam2(_myParam2), myParam3(_myParam3)
    { }

    int GetMyParam1()
    {
        return myParam1;
    }

    void SetMyParam1(int newMyParam1)
    {
        myParam1 = newMyParam1;
    }

    double GetMyParam2()
    {
        return myParam2;
    }

    void SetMyParam2(double newMyParam2)
    {
        myParam2 = newMyParam2;
    }

    bool GetMyParam3()
    {
        return myParam3;
    }

    void SetMyParam3(bool newMyParam3)
    {
        myParam3 = newMyParam3;
    }
};

class Vector
{
public:
    Vector() : x(0),y(0),z(0)
    { }
    Vector(double _x, double _y, double _z) : x(_x),y(_y), z(_z)
    { }

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }

    double GetVectorLength()
    {
        return sqrt(x * x + y * y + z * z);
    }

private:
    double x;
    double y;
    double z;

};


int main()
{
    MyClass myclass(5, 10.4, true);
    Vector vector(2, 6, 3);

    std::cout << myclass.GetMyParam1() << '\n';
    std::cout << myclass.GetMyParam2() << '\n';
    std::cout << myclass.GetMyParam3() << '\n';
    std::cout << vector.GetVectorLength() << '\n';

}
