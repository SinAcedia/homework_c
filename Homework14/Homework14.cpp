﻿
#include <iostream>
#include <string>

int main()
{
    std::string Test_String("");

    std::cout << "Enter any string\n";
    std::getline(std::cin, Test_String);

    std::cout << "\n" << "String: "<< Test_String << "\n";
    std::cout << "Length string: "<<Test_String.length() << "\n";

    std::cout << "First character: " << Test_String[0] << "\n";
    std::cout << "Last character: " << Test_String[Test_String.length()-1] << "\n";

}
