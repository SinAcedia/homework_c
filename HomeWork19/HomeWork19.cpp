﻿#include <iostream>
#include "Cat.h"
#include "Cow.h"
#include "Dog.h"
#include <array>

int main()
{
    const int countAnimal = 5;
    Animal* animal[countAnimal] = { new Cat(), new Cow(), new Dog(), new Cat(), new Dog() };
 
    for (Animal* a : animal)
    {
        a->Voice();
    }

}