#ifndef Animal_H_INCLUDED
#define Animal_H_INCLUDED
class Animal
{
public:
	Animal();
	virtual void Voice();
};

#endif // Animal_H_INCLUDED