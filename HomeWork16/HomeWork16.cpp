﻿#include <iostream>
#include <array>
#include <ctime>


int main()
{
    const int razmernost_a = 5;
    int Array_a[razmernost_a][razmernost_a];
    int a = 0;
    int sum_j = 0;
    time_t seconds = time(NULL);
    tm* timeinfo = new tm;
    localtime_s(timeinfo, &seconds);

    for (int i = 0; i < razmernost_a; i++)
    {
        for (int j = 0; j < razmernost_a; j++)
        {
            Array_a[i][j] = i + j;
            std::cout << Array_a[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    a = timeinfo->tm_mday % razmernost_a;
    a = (a >= razmernost_a) ? razmernost_a - 1 : a;

    for (int j = 0; j < razmernost_a; j++)
    {
        sum_j += Array_a[a][j];
            
    }
    std::cout << "Sum of line items " << a <<" = " <<sum_j << '\n';
}
