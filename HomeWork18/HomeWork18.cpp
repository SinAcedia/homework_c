﻿#include <iostream>
#include "Stack.h"

int main()
{
    std::cout << "Enter size array!\n";
    int sizeArray = 0;
    std::cin >> sizeArray;
    Stack<int> my(sizeArray);

    std::cout << "Enter " << sizeArray <<" values " << " " << typeid(my).name() <<"\n";
    int value = 0;
    while (my.Count() < sizeArray-1)
    {
        std::cin >> value;
        my.Push(value);
    }

    my.Pop();
    my.Pop();
    my.Pop();
    std::cout << "Enter new value!\n";
    std::cin >> value;
    my.Push(value);
    my.Pop();
    my.Pop();
    my.Pop();
}
