#include <iostream>

template<typename T>
class Stack
{
private:
    int stackSize = 0;
    int last = 0;
    T* stackPoint = nullptr;

public:
    void Pop();
    void Push(T value);
    int Count();
    Stack();
    ~Stack();
    Stack(int size);
};



template<typename T> void Stack<T>::Pop()
{
    if (last > -1)
    {
        std::cout << stackPoint[last] << "\n";
        stackPoint[last] = 0;
        last--;
    }
}
template<typename T> void Stack<T>::Push(T value)
{
    if (last < stackSize)
    {
        last++;
        stackPoint[last] = value;
    }
}
template<typename T> int Stack<T>::Count()
{
    return last;
}
template<typename T> Stack<T>::Stack()
{
    stackSize = 5;
    last = 0;
    stackPoint = new T[stackSize];
}
template<typename T> Stack<T>::Stack(int size)
{
    if (size > 0)
    {
        stackPoint = new T[size];
        stackSize = size;
        last = -1;
    }
    else
    {
        std::cout << "Incorrect size array";
    }
}

template<typename T> Stack<T>::~Stack()
{
    delete[] stackPoint;
}